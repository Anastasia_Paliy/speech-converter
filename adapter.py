from pocketsphinx import Pocketsphinx, AudioFile


def connect_PocketSphinx(config):
    return Pocketsphinx(**config)

def connect_AudioFile(config):
    return AudioFile(**config)
