from tkinter import *
from tkinter import font
from tkinter import filedialog as fd
import os
import time
from adapter import connect_PocketSphinx, connect_AudioFile
from ch_file import choose_file, choose_directory, open_file, saving, new_font, default_values
from return_filename import get_filename_1, get_filename

    
def SR_window():
    
    global fr0, fr1, state_label, l1, root1, font1, decoder_type

    root.withdraw()
    root1 = Toplevel(root)
    root1.geometry('800x500+0+0')
    root1.resizable(0,0)
    root1.title("Speech Recognition")

    font1 = new_font()
    
    decoder_type = StringVar()
    decoder_type.set('ps')
    
    fr0 = Frame(root1, padx = 10)
    fr0.pack(side = LEFT)
    fr1 = Frame(root1)
    fr1.pack(side = LEFT)
    fr0menu = Frame(fr0, pady = 10)
    fr0menu.pack(side = TOP)
    fr0rb = Frame(fr0)
    fr0rb.pack(side = TOP)
    fr0chf = Frame(fr0, pady = 10)
    fr0chf.pack(side = TOP)
    fr0dic = Frame(fr0, pady = 5)
    fr0dic.pack(side = TOP)
    fr0lm = Frame(fr0, pady = 5)
    fr0lm.pack(side = TOP)
    fr0hmm = Frame(fr0, pady = 5)
    fr0hmm.pack(side = TOP)
    fr0apply = Frame(fr0, pady = 7)
    fr0apply.pack(side = TOP)
    fr1file = Frame(fr1, pady = 5)
    fr1file.pack(side = TOP)
    fr1text = Frame(fr1)
    fr1text.pack(side = TOP)
    fr1bottom = Frame(fr1, pady = 7)
    fr1bottom.pack(side = TOP)
    fr1save = Frame(fr1bottom)
    fr1save.pack(side = RIGHT)
    fr1sl = Frame(fr1bottom)
    fr1sl.pack(side = RIGHT)
        
    b15 = Button(fr0menu, text = "Menu", command = to_menu)
    b15.pack(side = LEFT)
    
    b6 = Button(fr0chf, text = "Choose file", command = lambda: recognition(text1), padx = 27, font = font1)
    b6.pack(side = TOP, fill = X)
    
    l2 = Label(fr0dic, text = 'Dictionary')
    l2.pack(side = TOP)
    l3 = Label(fr0dic, text = get_filename(dic), fg = 'blue')
    l3.pack(side = TOP)   
    b7 = Button(fr0dic, text = "Change", command = lambda: choose_dict(l3))
    b7.pack(side = TOP)
    
    l4 = Label(fr0lm, text = 'Language model')
    l4.pack(side = TOP)
    l5 = Label(fr0lm, text = get_filename(lm), fg = 'blue')
    l5.pack(side = TOP)    
    b8 = Button(fr0lm, text = "Change", command = lambda: choose_lm(l5))
    b8.pack(side = TOP)

    l6 = Label(fr0hmm, text = 'Acoustic model')
    l6.pack(side = TOP)
    l7 = Label(fr0hmm, text = get_filename(hmm), fg = 'blue')
    l7.pack(side = TOP)    
    b9 = Button(fr0hmm, text = "Change", command = lambda: choose_hmm(l7))
    b9.pack(side = TOP)

    
    b11 = Button(fr0apply, text = 'Apply', command = lambda: updating(), padx = 50, font = font1)
    b11.pack(side = BOTTOM, fill = X)

    l1 = Label(fr1file, text = 'Unknown', fg = 'blue', font = font1)
    l1.pack(side = RIGHT)

    text1=Text(fr1text, width=70, height=22, wrap = WORD, autoseparators = True)
    text1.pack(side=LEFT)
    scroll = Scrollbar(fr1text, command=text1.yview)
    scroll.pack(side=LEFT, fill=Y)
    text1.config(yscrollcommand=scroll.set)

    state_label = Label(fr1sl, text = '', fg = 'blue')
    state_label.pack(side = LEFT)

    b12 = Button(fr1save, text = 'Save as', command = lambda: saving(text1), font = font1)
    b12.pack(side = RIGHT)
    
    rb1 = Radiobutton(fr0rb, variable = decoder_type, value='ps', text='PocketSphinx', command = ch_n_d)
    rb1.pack(side=TOP)
    rb2 = Radiobutton(fr0rb, variable = decoder_type, value='af', text='AudioFile', command = ch_n_d)
    rb2.pack(side=TOP)


def ch_n_d():
    global new_decoder, l1
    new_decoder = False
    l1.configure(text = 'Unknown')
    root.update()
    return new_decoder

    
def to_menu():
    global root, root1
    root.deiconify()
    root1.withdraw()


def create_label(ltext):
    global state_label
    state_label.configure(text = ltext)
    return (state_label)


def choose_dict(l3):
    global dic, root, new_decoder
    try_dic = choose_file()
    if try_dic != None:
        dic = try_dic
        l3.configure(text = get_filename_1(dic))
        new_decoder = False
        l1.configure(text = 'Unknown')
        state_label = create_label('')
        root.update()
    return dic


def choose_lm(l5):
    global lm, root, new_decoder
    try_lm = choose_file()
    if try_lm != None:
        lm = try_lm
        l5.configure(text = get_filename_1(lm))
        new_decoder = False
        l1.configure(text = 'Unknown')
        state_label = create_label('')
        root.update()
    return lm


def choose_hmm(l7):
    global hmm, root, new_decoder
    try_hmm = choose_directory()
    if try_hmm != None:
        hmm = try_hmm
        l7.configure(text = get_filename_1(hmm))
        new_decoder = False
        l1.configure(text = 'Unknown')
        state_label = create_label('')
        root.update()
    return hmm


def updating():

    global ps, new_decoder, decoder_type, state_label, model_path
        
    config = {'hmm': os.path.join(model_path, hmm),
              'lm': os.path.join(model_path, lm),
              'dict': os.path.join(model_path, dic)}

    state_label = create_label('Updating')    
    root.update()
    
    if decoder_type.get() == 'ps':
        time1 = time.monotonic()
        ps = connect_PocketSphinx(config)
        print('For updating decoder: ', time.monotonic()-time1)
        
    new_decoder = True
    state_label = create_label('Ready')    
    
    
def recognition(text1):
    
    global ps, new_decoder, state_label, l1, decoder_type, model_path

    audio = choose_file()

    l1.configure(text = get_filename_1(audio))
    print(get_filename_1(audio))
    root.update()

    if decoder_type.get() == 'ps':
        if new_decoder == True:
            state_label = create_label('Decoding')
            root.update()
            time2 = time.monotonic()
            text1.insert(text1.index(END), ps.decode(audio_file = audio))
            text1.insert(text1.index(END), ' ')
              
        else:
            a = updating()
            state_label = create_label('Decoding')
            root.update()
            time2 = time.monotonic()
            text1.insert(text1.index(END), ps.decode(audio_file = audio))
            text1.insert(text1.index(END), ' ')


    else:
        config = {'audio_file': audio,
                  'hmm': os.path.join(model_path, hmm),
                  'lm': os.path.join(model_path, lm),
                  'dict': os.path.join(model_path, dic)}

        state_label = create_label('Updating')
        root.update()
        time1 = time.monotonic()
        
        ps = connect_AudioFile(config)
        
        print('For updating: ', time.monotonic()-time1)

        state_label = create_label('Decoding')
        root.update()
        time2 = time.monotonic()
        
        for phrase in ps:
            text1.insert(text1.index(END), phrase)
            text1.insert(text1.index(END), ' ')
            root.update()
    
    print('For decoding: ', time.monotonic()-time2)
    print('')
    state_label = create_label('Done')

def to_menu2(root2):
    global root
    root.deiconify()
    root2.withdraw()

def MF_window():
    
    global root, font1, l11

    root.withdraw()
 
    root2 = Toplevel(root)
    root2.geometry('700x500+0+0')
    root2.resizable(0,0)
    root2.title("My files")

    font1 = new_font()
    
    fr10 = Frame(root2)
    fr10.pack(side = TOP, pady = 5)
    fr11 = Frame(root2)
    fr11.pack(side = TOP, pady = 5)
    fr12 = Frame(root2)
    fr12.pack(side = TOP)
    
    l11 = Label(fr10, text = 'Unknown', fg = 'blue', font = font1)
    l11.pack(side = TOP)
    b15 = Button(fr10, text = "Menu", command = lambda: to_menu2(root2))
    b15.pack(side = LEFT)
    b6 = Button(fr10, text = "Choose file", command = lambda: open_file(text2, l11, root2))
    b6.pack(side = LEFT)
    
    text2=Text(fr11, width=60, height=20, wrap = WORD, autoseparators = True)
    text2.pack(side=LEFT)
    scroll = Scrollbar(fr11, command=text2.yview)
    scroll.pack(side=LEFT, fill=Y)
    text2.config(yscrollcommand=scroll.set)
    
    b16 = Button(fr12, text = 'Save as', command = lambda: saving(text2))
    b16.pack(side = RIGHT)
    



global hmm, lm, dic, root, new_decoder, decoder_type, model_path

hmm, lm, dic = default_values()
new_decoder = False
model_path = os.getcwd()+'/model'

root = Tk()
root.title ("Speech Converter")
root.resizable(0,0)

b1=Button(root,text='Speech Recognition', command = SR_window)
b1.pack(fill=X)

b2=Button(root,text='My files', command = MF_window)
b2.pack(fill=X)

b3=Button(root,text='Settings')
b3.pack(fill=X)

b4=Button(root,text='Help')
b4.pack(fill=X)

b5=Button(root,text='Exit', command = root.destroy)
b5.pack(fill=X)


root.mainloop()
