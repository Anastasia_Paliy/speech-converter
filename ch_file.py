from tkinter import *
from tkinter import filedialog as fd
from tkinter import font
from return_filename import get_filename_1, get_filename
from os import getcwd


directory = getcwd()


def choose_file():
    
    file_name = fd.askopenfilename(title = 'Choose file', initialdir = directory)
    if file_name:
        return file_name
    return None


def choose_directory():
    
    dir_name = fd.askdirectory(title = 'Choose directory', initialdir = directory)
    if dir_name:
        return dir_name
    return None

    
def open_file(text2, l11, root2):
    
    file_name = fd.askopenfilename()
    
    with open(file_name, encoding = 'utf-8') as file:
        contents=file.read(300)
        
    text2.insert(text2.index(END), contents)
    l11.configure(text = get_filename_1(file_name))
    root2.update()


def saving(text1):

    contents = text1.get(1.0, END)
    
    new_file = fd.asksaveasfilename(title = 'Save file as', initialdir = directory)

    file = open(new_file, 'w')
    file.write(contents)
    file.close()


def new_font():
    my_font = font.Font(size = 12)
    return (my_font)


def default_values():
    hmm = 'zero_ru.cd_cont_4000'
    lm = 'my_lm1.lm.bin'
    dic = 'my_dict1.dic'
    return hmm, lm, dic

